{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  gems = bundlerEnv {
    name = "scrapers";

    inherit ruby;
    # expects Gemfile, Gemfile.lock and gemset.nix in the same directory
    # > nix-shell -p bundler --run 'bundle lock --update'
    # > nix-shell -p bundix --run bundix
    gemdir = ./.;
  };
  wrapScript = name: script:
    stdenv.mkDerivation {
      inherit name script;
      buildInputs = [ gems.wrappedRuby ];

      buildCommand = ''
        install -D -m755 ${script} $out/bin/${name}
        patchShebangs $out/bin/${name}
      '';

      meta.mainProgram = name;
    };
in {
  xerox = wrapScript "xerox" ./xerox/scrape.rb;
  luftdaten = wrapScript "luftdaten" ./luftdaten/scrape.rb;
  luftqualitaet = wrapScript "luftqualitaet" ./luftqualitaet/scrape.rb;
  fhem = wrapScript "fhem" ./fhem/scrape.rb;
  matemat = wrapScript "matemat" ./matemat/scrape.rb;
  freifunk_node = wrapScript "freifunk_node" ./freifunk_node/scrape.rb;
  impfee = wrapScript "impfee" ./impfee/scrape.rb;
  riesa-efau-kalender = wrapScript "riesa-efau-kalender" ./riesa-efau-kalender/scrape.rb;
  kreuzchor-termine = wrapScript "kreuzchor-termine" ./kreuzchor-termine/scrape.rb;
  dhmd-veranstaltungen = wrapScript "dhmd-veranstaltungen" ./dhmd-veranstaltungen/scrape.rb;
  mkz-programm = wrapScript "mkz-programm" ./mkz-programm/scrape.rb;
  drk-impfaktionen = wrapScript "drk-impfaktionen" ./drk-impfaktionen/scrape.rb;
  zuendstoffe = wrapScript "zuendstoffe" ./zuendstoffe/scrape.rb;
  dresden-versammlungen = wrapScript "dresden-versammlungen" ./dresden-versammlungen/scrape.rb;
  azconni = wrapScript "azconni" ./azconni/scrape.rb;
  kunsthaus = wrapScript "kunsthaus" ./kunsthaus/scrape.rb;
  hfbk-dresden = wrapScript "hfbk-dresden" ./hfbk-dresden/scrape.rb;
  staatsoperette = wrapScript "staatsoperette" ./staatsoperette/scrape.rb;
  tjg-dresden = wrapScript "tjg-dresden" ./tjg-dresden/scrape.rb;
  dresden-kulturstadt = wrapScript "dresden-kulturstadt" ./dresden-kulturstadt/scrape.rb;
  nabu = wrapScript "nabu" ./nabu/scrape.rb;
  museen-dresden = wrapScript "museen-dresden" ./museen-dresden/scrape.rb;
  criticalmass = wrapScript "criticalmass" ./criticalmass/scrape.rb;
  dearfuturedresden = wrapScript "dearfuturedresden" ./dearfuturedresden/scrape.rb;
  rauze = wrapScript "rauze" ./rauze/scrape.rb;
  dresden-ikt = wrapScript "dresden-ikt" ./dresden-ikt/scrape.rb;
  dresdencontemporaryart = wrapScript "dresdencontemporaryart" ./dresdencontemporaryart/scrape.rb;
  htw-dresden = wrapScript "htw-dresden" ./htw-dresden/scrape.rb;
  bibo-dresden = wrapScript "bibo-dresden" ./bibo-dresden/scrape.rb;
  johannstadthalle = wrapScript "johannstadthalle" ./johannstadthalle/scrape.rb;
  johannstadt = wrapScript "johannstadt" ./johannstadt/scrape.rb;
  kosmotique = wrapScript "kosmotique" ./kosmotique/scrape.rb;
  zwickmuehle = wrapScript "zwickmuehle" ./zwickmuehle/scrape.rb;
  sowieso = wrapScript "sowieso" ./sowieso/scrape.rb;
  solar = wrapScript "solar" ./solar/scrape.rb;

  cccamp23-schedules = ./camp23-schedules;
}
