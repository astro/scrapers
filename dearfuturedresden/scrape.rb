#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'nokogiri'
require 'erb'

def fmt_time t
  t.strftime "%Y%m%dT%H%M%S"
end

class Event
  attr_accessor :title, :url, :location, :dtstart, :dtend
end

events = []

url = "https://dearfuturedresden.de/festival2024/programm"
doc = Nokogiri::HTML URI.open(url)
doc.css(".uk-container").each do |content|
  ev = Event::new
  ev.title = content.css("h2").text.strip
  ev.url = content.css("h2 a").attr('href')
  ev.location = content.css(".uk-text-left.uk-panel").text.strip.gsub(/[\r\n]/, ", ")
  date = nil
  time = nil
  content.css(".uk-link-text").each do |div|
    text = div.text.strip
    if text =~ /(\d\d)\.(\d\d)\.(\d\d\d\d)/
      date = [$3.to_i, $2.to_i, $1.to_i]
    elsif text =~ /(\d\d):(\d\d)/
      time = [$2.to_i, $1.to_i]
    end
  end
  next unless date and time
  ev.dtstart = Time::local date[0], date[1], date[2], time[1], time[0], 0
  ev.dtend = ev.dtstart + 7200
  next unless ev.url and ev.dtstart and ev.title

  events << ev
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  BEGIN:VEVENT
  SUMMARY:<%= ev.title %>
  DTSTART:<%= fmt_time ev.dtstart %>
  DTEND:<%= fmt_time ev.dtend %>
  UID:<%= ev.url %>
  URL:<%= ev.url %>
  LOCATION:<%= ev.location %>
  END:VEVENT
  <% end %>
  END:VCALENDAR
EOF

puts ical.result
