#!/usr/bin/env ruby
# coding: utf-8

require 'time'
require 'open-uri'
require 'json'
require 'erb'

def fmt_time event, key
  t = Time.parse event["Datum"]
  times = nil
  if event["Zeit"] =~ /(\d{1,2})[\.:]?(\d{0,2})\s*-\s*(\d{1,2})[\.:]?(\d{0,2})/
    start = t.clone() + $1.to_i * 3600 + $2.to_i * 60
    stop = t.clone() + $3.to_i * 3600 + $4.to_i * 60
  else
    start = t.clone()
    stop = t.clone() + 21 * 3600 + 59 * 60
  end
  { :start => start, :stop => stop }[key].strftime "%Y%m%dT%H%M%S"
end

url = "https://www.dresden.de/data_ext/versammlungsuebersicht/Versammlungen.json"
data = URI.open(url).read()
json = JSON.parse data
events = json['Versammlungen'].select { |event| event["Status"] == "beschieden" }

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  BEGIN:VEVENT
  METHOD:PUBLISH
  CLASS:PUBLIC
  UID:<%= fmt_time ev, :start %>
  DTSTART:<%= fmt_time ev, :start %>
  DTEND:<%= fmt_time ev, :stop %>
  SUMMARY:<%= ev["Thema"] %>
  LOCATION:<%= ev["Veranstalter"] %> (<%= ev["Teilnehmer"] %>), <%= ev["Ort"] %>
  URL:https://www.dresden.de/de/rathaus/dienstleistungen/versammlungsuebersicht.php
  END:VEVENT
  <% end %>
  END:VCALENDAR
EOF

puts ical.result
