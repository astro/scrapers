#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'nokogiri'
require 'erb'
require 'uri'

events = []
seen_urls = {}

url = "https://www.frauen-ev-sowieso.de/aktuelle-veranstaltungen/"
STDERR.puts "GET HTML #{url}"
doc = Nokogiri::HTML URI.open(url)
doc.css(".mec-event-title a").each do |a|
  link = URI.join(url, a.attr("href")).to_s

  STDERR.puts "GET HTML #{link}"
  event_doc = Nokogiri::HTML URI.open(link)
  ics_link = event_doc.css(".mec-events-gcal[contains('iCal')]").attr("href")

  STDERR.puts "GET ICS #{ics_link}"
  ics = URI.open(ics_link).read
  if ics =~ /BEGIN:VEVENT(.+)END:VEVENT/m
    ev = "BEGIN:VEVENT\n#{$1.strip}\nURL:#{link}\nEND:VEVENT\n"
    events << ev
  else
    STDERR.puts "No event found in ICS"
  end
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  <%= ev %>
  <% end %>
  END:VCALENDAR
EOF

puts ical.result
