#! /usr/bin/env ruby

require 'date'
require 'open-uri'
require 'csv'
require 'influxdb'

db = InfluxDB::Client.new(url: "http://grafana.serv.zentralwerk.org:8086/solar", time_precision: "m")

HOST = ARGV.shift
USER = ARGV.shift
PASSWORD = ARGV.shift
BASE_URL = "ftp://#{USER}:#{PASSWORD}@#{HOST}"

(1..2).each do |a|
  (1..6).each do |s|
    (0..3).each do |day_offset|
      date = Date.today - day_offset
      url = "#{BASE_URL}/B8_A#{a}_S#{s}_global_#{date.month}_#{date.day}_#{date.year}.txt"
      STDERR.puts "GET #{url}"
      begin
        f = URI.open(url).read.gsub(/\r/, "")
      rescue Net::FTPError
        puts $!
        next
      end
      csv = CSV.new f, col_sep: ';', headers: true

      points = csv.collect do |row|
        {
          series: "solar",
          tags: {
            address: a, #row["address"],
            bus: row["bus"],
            strings: s, #row["strings"],
          },
          values: {
            pac: row["pac"].to_i,
            pdc: row["pdc"].to_i,
            udc: row["udc"].to_i,
            temp: row["temp"].to_i,
          },
          timestamp: row["timestamp"].to_i,
        }
      end
      STDERR.puts "Writing #{points.length} points"
      db.write_points points, 's'
    end
  end
end
