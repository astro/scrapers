#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'nokogiri'
require 'uri'
require 'json'
require 'erb'

def fmt_time t
  t.strftime "%Y%m%dT%H%M%S"
end

class Event
  attr_accessor :title, :url, :dtstart, :dtend

  def initialize
  end
end

json_events = []

url = "http://www.johannstadthalle.de/programm.html"
STDERR.puts "GET #{url}"
URI.open(url).read.scan(/var eventsInline = \[(.+?),\s*\];/m) do |m|
  json = m[0].gsub('"', '\\"')
           .gsub("date:", '"date":')
           .gsub(/'(\w+)':/, '"\1":')
           .gsub(/:\s*'/, ':"')
           .gsub(/'\s*([,\}])/, '"\1')
  json_events += JSON.parse("[#{json}]")
end

events = []

json_events.each do |json|
  begin
    ev = Event::new
    ev.title = URI::decode_uri_component json['title']
    ev.url = json['url']
    ev.dtstart = Time.at(json['date'] / 1000)
    ev.dtend = ev.dtstart + 7200
    events << ev
  rescue
    STDERR.puts "Omitting: #{$!}"
    STDERR.puts $!.backtrace
  end
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  BEGIN:VEVENT
  SUMMARY:<%= ev.title %>
  DTSTART:<%= fmt_time ev.dtstart %>
  DTEND:<%= fmt_time ev.dtend %>
  UID:<%= ev.url %>
  URL:<%= ev.url %>
  END:VEVENT
  <% end %>
  END:VCALENDAR
EOF

puts ical.result
