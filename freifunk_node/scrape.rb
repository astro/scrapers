#!/usr/bin/env ruby

require 'open-uri'
require 'json'
require 'uri'
require 'influxdb'

HOST = ARGV[0]

data = JSON.parse URI::open("http://#{HOST}/sysinfo-json.cgi").read()

$node_id = data['data']['common']['node']
$node_name = URI::decode_uri_component(data['data']['contact']['name'])
               .gsub(/\+/, " ")

$points = []
$values = {}

def recurse(data, prefix='')
  new_prefix = proc do |name|
    if prefix == ""
      name
    else
      "#{prefix}.#{name}"
    end
  end

  if data.kind_of? Hash
    data.each do |k, v|
      recurse v, new_prefix.call(k)
    end
  elsif data.kind_of? Array
    data.zip(0..).each do |(v, i)|
      recurse v, new_prefix.call(i)
    end
  elsif data.kind_of? String and data != ""
    data.strip!
    data = data.split(/,/)
    if data.length > 1
      data.zip(0..).each do |(v, i)|
        recurse v, new_prefix.call(i)
      end
    else
      data = data[0].split(/ +/)
      if data.length > 1
        data.zip(0..).each do |(v, i)|
          recurse v, new_prefix.call(i)
        end
      else
        if data[0] =~ /^\d+$/
          $values[prefix] = data[0].to_f
        end
      end
    end
  elsif data.kind_of? Integer or data.kind_of? Float
    $values[prefix] = data.to_f
  end
end


recurse data['data']

$points += $values.map do |k, v|
  { series: 'node',
    tags: {
      node_id: $node_id,
      node_name: $node_name,
      key: k,
    },
    values: {
      value: v,
    },
  }
end


data['data']['bmxd']['links'].each do |link|
  pvalues = {}
  %w(rtq rq tq).each do |k|
    pvalues[k] = link[k].to_i
  end

  $points.push(
    { series: 'node_bmx_links',
      tags: {
        node_id: $node_id,
        node_name: $node_name,
        peer_id: link['node'],
      },
      values: pvalues,
    })
end

db = InfluxDB::Client.new(url: "http://grafana.serv.zentralwerk.org:8086/freifunk")
db.write_points $points
