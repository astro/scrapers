#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'json'
require 'nokogiri'
require 'erb'

events = []

url = "https://www.staatsoperette.de/spielplan/"
STDERR.puts "GET #{url}"
doc = Nokogiri::HTML URI.open(url)

doc.css(".performance").each do |content|
  event_link = URI::join url, content.css("h2 a").attr("href")
  ical_link = URI.join url, content.css("a[href$='.ics']").attr('href')

  event_line = false
  has_url = false
  STDERR.puts "GET #{ical_link}"
  ical = URI.open(ical_link).read
  events.push ical.lines.collect { |line|
    case line.chomp
    when "BEGIN:VEVENT"
      event_line = true
      line
    when "END:VEVENT"
      event_line = false
      if has_url
        line
      else
        ["URL:#{event_link}\n", line]
      end
    when /URL:/
      if event_line
        has_url = true
        line
      else
        []
      end
    else
      event_line ? line : []
    end
  }.flatten.join
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin

  <%= events.join "\n" %>

  END:VCALENDAR
EOF

puts ical.result
