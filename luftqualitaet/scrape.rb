#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'csv'
require 'time'
require 'influxdb'

TIME_PRECISION = 'h'

station = ARGV[0]
unless station
  puts "Missing station argument"
  exit 1
end

def fmt_date d
  d.strftime "%Y-%m-%d"
end
url = "https://www.umweltbundesamt.de/api/air_data/v2/airquality/csv?date_from=#{fmt_date(Date::today - 1)}&time_from=0&date_to=#{fmt_date Date::today}&time_to=24&station=#{station}&lang=de"

puts "GET #{url}"
data = open(url).read().force_encoding("UTF-8")

INDEX = {
  "sehr gut" => 1,
  "gut" => 2,
  "mäßig" => 3,
  "schlecht" => 4,
  "sehr schlecht" => 5,
}

points = []
CSV::parse(data, headers: true, col_sep: ";").each do |data|
  values = {}
  station = nil
  time = nil
  data.each do |n, v|
    n = n.dup.force_encoding "UTF-8"
    case n
    when /Stationscode/
      station = v
    when /Datum/
      time = Time.strptime v, "'%d.%m.%Y %H:%M'" if v
    when "Luftqualitätsindex"
      values["Luftqualitätsindex"] = INDEX[v] if INDEX[v]
    else
      values[n] = v.to_f
    end
  end

  points.push({
                series: "umweltbundesamt",
                tags: {
                  station: station,
                },
                values: values,
                timestamp: InfluxDB::convert_timestamp(time.to_i, TIME_PRECISION),
              }) if time
end

db = InfluxDB::Client.new(url: "http://grafana.serv.zentralwerk.org:8086/luftdaten")
db.write_points points, TIME_PRECISION
