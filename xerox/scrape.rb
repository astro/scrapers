#!/usr/bin/env ruby

require 'open-uri'
require 'influxdb'

HOST = ARGV[0]
USER = ARGV[1]
PASSWORD = ARGV[2]

def scrape_paper_supply
  results = {}

  URI.open("http://#{HOST}/sttray.htm", :http_basic_authentication => [USER, PASSWORD])
    .read()
    .scan(/\['Tray (.+?)',\d+,(\d+),/) do |m|

    results[m[0]] = m[1].to_i
  end

  results
end

def scrape_consumables
  results = {}

  doc = URI.open("http://#{HOST}/stsply.htm", :http_basic_authentication => [USER, PASSWORD])
          .read()
          .force_encoding("ISO-8859-1")
          .encode("utf-8", replace: nil)

  doc.scan(/info.concat\(\[\[(.+?)\);/) do |m|
    m[0].scan(/\['(.+?)',(\d+),(\d+)\]/) do |m|
      results[m[0]] = {
        :status => m[1].to_i,
        :life => m[2].to_i,
      }
    end
  end

  results
end

paper_supply = scrape_paper_supply
consumables = scrape_consumables

db = InfluxDB::Client.new(url: "http://grafana.serv.zentralwerk.org:8086/drucker")
db.write_points [ {
  series: "paper_supply",
  tags: { host: HOST },
  values: paper_supply,
}, {
  series: "consumables_status",
  tags: { host: HOST },
  values: consumables.inject({}) do |status, consumable|
    status[consumable[0]] = consumable[1][:status]
    status
  end,
}, {
  series: "consumables_life",
  tags: { host: HOST },
  values: consumables.inject({}) do |life, consumable|
    life[consumable[0]] = consumable[1][:life]
    life
  end,
} ], 'm'
