#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'nokogiri'
require 'erb'
require 'uri'

events = []

url = "https://museen-dresden.de/programm/kalender"
STDERR.puts "GET #{url}"
doc = Nokogiri::HTML URI.open(url)
links = doc.css("a[href$='.ics']")
links.each_with_index do |a, i|
  ics_url = URI.join url, a.attr("href")
  STDERR.puts "[#{i + 1}/#{links.length}] GET #{ics_url}"
  ics = URI.open(ics_url).read
  if ics =~ /BEGIN:VEVENT(.+)END:VEVENT/m
    ev = "BEGIN:VEVENT\n#{$1.strip}\nEND:VEVENT\n"
    events << ev
  else
    STDERR.puts "No iCal in #{ics_url}"
  end
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  <%= ev %>
  <% end %>
  END:VCALENDAR
EOF

puts ical.result
