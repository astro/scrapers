#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'json'
require 'nokogiri'
require 'erb'

events = []

url = "https://dresden-kulturstadt.de/ksdd_events/"
STDERR.puts "GET #{url}"
doc = Nokogiri::HTML URI.open(url)

doc.css(".btn-ics").each do |a|
  ical_link = URI.join url, a.attr("href")

  event_line = false
  has_url = false
  STDERR.puts "GET #{ical_link}"
  ical = URI.open(ical_link).read
  events.push ical.lines.collect { |line|
    case line.chomp
    when "BEGIN:VEVENT"
      event_line = true
      line
    when "END:VEVENT"
      event_line = false
      line
    else
      event_line ? line : []
    end
  }.flatten.join
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin

  <%= events.join "\n" %>

  END:VCALENDAR
EOF

puts ical.result
